/**
 * Manages all the configs both for the whole client and each server
 * 
 * Exports:
 *  Server Config
 *   - AddModuleConfig(module_file, readFunc) - Adds the read function for a server module config
 *   - GetConfig(serverID) - Read or get the server config
 *   - GetModuleConfig(serverID, module_file) - Get the module server config
 *   - SaveConfig - Save a server config file
 * 
 *  Main Config
 *   - AddModuleConfigMain(module_file, readFunc) - Adds the read function for a main module config
 *   - GetConfigMain() - Read or get the main config
 *   - GetModuleConfigMain - Get the main module config
 *   - SaveConfigMain - Save the main config file
 */
const fs = require("fs")
const path = require("path")

// Conts to help get config location
const config_folder = path.join(__dirname, "config")
const filename_main = path.join(config_folder, "main.json")

// Hold all the info the load the module configs
var moduleConfigs = {}
var moduleConfigsMain = {}

// Holds all the config's data
var data = {}

// Adds the read function for a server module config
function AddModuleConfig(module_file, readFunc) {
    let basename = path.basename(module_file) // Get the filename for the module
    moduleConfigs[basename.substr(0, basename.length - 3)] = readFunc // Store the function in the object
}

// Read or get the server config
function GetConfig(serverID) {
    let filename = path.join(config_folder, serverID + ".json") // Get what the filename should be

    if (!(filename in data)) { // If the file is not cached, load it
        // Read the file and set the base for the new data
        let rawData = readConfig(filename)
        let newData = {
            modules: {}
        }

        // Loop through and load all of the module configs
        for (let m in moduleConfigs) {
            if (rawData === undefined || rawData.modules === undefined) { // If the raw data is undefined or the modules are send no data
                newData.modules[m] = moduleConfigs[m](undefined)
            } else { // Else send the raw config data
                newData.modules[m] = moduleConfigs[m](rawData.modules[m])
            }
        }

        // Cache the data and save the config
        data[filename] = newData
        saveConfig(filename)
    }

    return data[filename] // Return with the data
}

// Get the module server config
function GetModuleConfig(serverID, module_file) {
    let basename = path.basename(module_file) // Get the name for the module
    return GetConfig(serverID).modules[basename.substr(0, basename.length - 3)] // Get the config for the server then pull the module config
}

// Save a server config file
function SaveConfig(serverID) {
    return saveConfig(path.join(config_folder, serverID + ".json"))
}

// Adds the read function for a main module config
function AddModuleConfigMain(module_file, readFunc) {
    let basename = path.basename(module_file) // get the name for the module
    moduleConfigsMain[basename.substr(0, basename.length - 3)] = readFunc // Store the function in the object
}

// Read or get the main config
function GetConfigMain() {
    if (!(filename_main in data)) { // If the file is not cached, load it
        // Read the file and set the base for the new data
        let rawData = readConfig(filename_main)
        let newData = {
            modules: {},
            token: "<token here>"
        }

        if (rawData !== undefined && rawData.token !== undefined) { // If the token is in the raw data read it
            newData.token = rawData.token
        }

        // Loop through and load all of the module configs
        for (let m in moduleConfigsMain) {
            if (rawData === undefined || rawData.modules === undefined) { // If the raw data is undefined or the modules are send no data
                newData.modules[m] = moduleConfigsMain[m](undefined)
            } else { // Else send the raw config data
                newData.modules[m] = moduleConfigsMain[m](rawData.modules[m])
            }
        }

        // Cache the data and save the config
        data[filename_main] = newData
        saveConfig(filename_main)
    }

    return data[filename_main] // Return with the data
}

// Get the main module config
function GetModuleConfigMain(module_file) {
    let basename = path.basename(module_file) // Get the name for the module
    return GetConfigMain().modules[basename.substr(0, basename.length - 3)] // Get the main config then pull the module config
}

// Save the main config file
function SaveConfigMain() {
    saveConfig(filename_main)
}

module.exports = {
    AddModuleConfig,
    AddModuleConfigMain,
    GetConfig,
    GetConfigMain,
    GetModuleConfig,
    GetModuleConfigMain,
    SaveConfig,
    SaveConfigMain
}

// Read config from a file
function readConfig(filename) {
    if (!fs.existsSync(filename)) { // If the file does not exist return null
        return null
    }

    let filedata = fs.readFileSync(filename) // Otherwise read and parse it
    return JSON.parse(filedata)
}

// Save a config to a file
function saveConfig(filename) {
    if (!(filename in data)) { // If the file is not already loaded return
        return
    }

    if (!fs.existsSync(path.dirname(filename))) { // If the golder does not exit create it
        fs.mkdirSync(path.dirname(filename), {
            recursive: true
        })
    }

    fs.writeFileSync(filename, JSON.stringify(data[filename], null, 4)) // Actually write to the file
}