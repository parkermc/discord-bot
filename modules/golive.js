// TODO add better mutliple message protection
const Discord = require("discord.js")
const request = require('request');

const ConfigManager = require("../config_manager")
const EventListener = require("../event_listener")
const Commands = require("./commands")

const offlineResetSeconds = 60

let guildChannelCache = {}
let liveIdsOfflineTime = {}

function pad(num, size) {
    var s = "000000000" + num;
    return s.substr(s.length - size);
}

function commandSetGoLiveChannel(client, msg, args, admin) {

    let channel = undefined
    if (args[0] == "") {
        channel = msg.channel
    } else {
        channel = Commands.ArgChannel(args[0], msg.guild)
    }
    if (channel !== undefined) {
        ConfigManager.GetModuleConfig(msg.guild.id, __filename).channel = channel.id
        ConfigManager.SaveConfig(msg.guild.id)
        msg.channel.send("", new Discord.RichEmbed() // Send output message
            .setColor("#00ff00")
            .setDescription("Go Live channel set.")
            .addField("Name", channel.name, true)
            .addField("ID", channel.id, true)
        )
    } else {
        msg.channel.send("", new Discord.RichEmbed() // Send not found error message
            .setColor("#FF0000")
            .setTitle("Error")
            .setDescription(`Could not find a channel mentioned in the command.`)
        )
    }
}

function commandSetGoLiveRole(client, msg, args, admin) {

    let roles = Commands.ArgRoles(args[0], msg.guild)
    if (roles !== undefined) {
        let configRoles = []

        let embed = new Discord.RichEmbed() // start the embed
            .setColor("#00ff00")
            .setDescription("Go Live role set.")

        let first = true
        for (let role of roles) {
            if (first) {
                first = false
            } else {
                embed.addBlankField(false)
            }
            embed.addField("Name", role.role.name, true)
                .addField("ID", role.role.id, true)
                .addField("Mode", role.mode, true)

            configRoles.push({
                id: role.role.id,
                mode: role.mode
            })
        }

        msg.channel.send("", embed) // Send output message

        ConfigManager.GetModuleConfig(msg.guild.id, __filename).roles = configRoles
        ConfigManager.SaveConfig(msg.guild.id)

    } else {
        msg.channel.send("", new Discord.RichEmbed() // Send not found error message
            .setColor("#FF0000")
            .setTitle("Error")
            .setDescription(`Error while finding role(s) matching \`${args[0]}\`.`)
        )
    }
}


function commandSetGoLiveDescription(client, msg, args, admin) {
    ConfigManager.GetModuleConfig(msg.guild.id, __filename).description = args[0]
    ConfigManager.SaveConfig(msg.guild.id)
    msg.channel.send("", new Discord.RichEmbed() // Send output message
        .setColor("#00ff00")
        .setDescription("Go Live message description set.")
    )
}

function commandSetGoLiveMessage(client, msg, args, admin) {
    ConfigManager.GetModuleConfig(msg.guild.id, __filename).message = args[0]
    ConfigManager.SaveConfig(msg.guild.id)
    msg.channel.send("", new Discord.RichEmbed() // Send output message
        .setColor("#00ff00")
        .setDescription("Go Live message set.")
    )
}

function commandSetGoLiveTitle(client, msg, args, admin) {
    ConfigManager.GetModuleConfig(msg.guild.id, __filename).title = args[0]
    ConfigManager.SaveConfig(msg.guild.id)
    msg.channel.send("", new Discord.RichEmbed() // Send output message
        .setColor("#00ff00")
        .setDescription("Go Live message title set.")
    )
}


function onPresenceUpdate(client, oldMember, newMember) {
    let config = ConfigManager.GetModuleConfig(newMember.guild.id, __filename)

    if (liveIdsOfflineTime[newMember.id] == -1) {
        if (newMember.presence.status != "offline" && (newMember.presence.game == null || (!newMember.presence.game.streaming))) {
            liveIdsOfflineTime[newMember.id] = Date.now() + (offlineResetSeconds * 1000)
        }
    } else {
        if (liveIdsOfflineTime[newMember.id] === undefined || liveIdsOfflineTime[newMember.id] !== -1) {
            if (newMember.presence.game !== null && newMember.presence.game.streaming) {
                if (Commands.UserInRoles(newMember, config.roles)) { // TODO is in roles
                    if (liveIdsOfflineTime[newMember.id] === undefined || Date.now() > liveIdsOfflineTime[newMember.id]) {
                        sendLiveMsg(client, newMember)
                    }
                    liveIdsOfflineTime[newMember.id] = -1
                }
            }
        }
    }
}

function replaceText(text, game, url, username, title) {
    return text.replace("{{game}}", game).replace("{{url}}", url).replace("{{username}}", username).replace("{{title}}", title)
}

function sendLiveMsg(client, member) {
    let config = ConfigManager.GetModuleConfig(member.guild.id, __filename)
    let configMain = ConfigManager.GetModuleConfigMain(__filename)

    if (!(member.guild.id in guildChannelCache)) {
        for (let channel of client.channels) {
            if (channel[1].id == config.channel) {
                guildChannelCache[member.guild.id] = channel[1]
            }
        }
    }

    let game = member.presence.game.state
    let url = member.presence.game.url
    let username = member.user.username
    let title = member.presence.game.details
    let now = new Date(Date.now())
    if (member.presence.game.name == "Twitch") {
        let splitUrl = member.presence.game.url.split("/")
        let twitchUsername = splitUrl[splitUrl.length - 1]
        request(`https://api.twitch.tv/helix/users?login=${splitUrl[splitUrl.length-1]}`, {
            headers: {
                "Client-ID": configMain.twitchClientID
            },
            json: true
        }, (err, res, body) => {
            if (body !== undefined && body.data !== undefined && body.data.length > 0 && body.data[0].profile_image_url) {
                guildChannelCache[member.guild.id].send(replaceText(config.message, game, url, username, title), new Discord.RichEmbed()
                    .setTitle(replaceText(config.title, game, url, username, title))
                    .setDescription(replaceText(config.description, game, url, username, title))
                    .setThumbnail(body.data[0].profile_image_url)
                    .setImage(`https://static-cdn.jtvnw.net/previews-ttv/live_user_${twitchUsername}.jpg`)
                    .addField("Playing", game, true)
                    .addField("At", `${pad(now.getUTCHours(), 2)}:${pad(now.getUTCMinutes(), 2)} UTC`, true)
                    .setURL(url)
                    .setColor("#660197")
                )
            } else {
                guildChannelCache[member.guild.id].send(replaceText(config.message, game, url, username, title), new Discord.RichEmbed()
                    .setTitle(replaceText(config.title, game, url, username, title))
                    .setDescription(replaceText(config.description, game, url, username, title))
                    .setImage(`https://static-cdn.jtvnw.net/previews-ttv/live_user_${twitchUsername}.jpg`)
                    .addField("Playing", game, true)
                    .addField("At", `${pad(now.getUTCHours(), 2)}:${pad(now.getUTCMinutes(), 2)} UTC`, true)
                    .setURL(url)
                    .setColor("#660197")
                )
            }
            if (err) {
                return console.error(err);
            }
        });
    } else {
        guildChannelCache[member.guild.id].send(replaceText(config.message, game, url, username, title), new Discord.RichEmbed()
            .setTitle(replaceText(config.title, game, url, username, title))
            .setDescription(replaceText(config.description, game, url, username, title))
            .setURL(url)
        )
    }
}

function ReadConfig(rawData) {
    let newData = {
        channel: "",
        description: "{{title}}",
        message: "",
        roles: [],
        title: "{{username}} has gone live!"
    }

    if (rawData !== undefined) {
        if (rawData.channel !== undefined) {
            newData.channel = rawData.channel
        }
        if (rawData.description !== undefined) {
            newData.description = rawData.description
        }
        if (rawData.message !== undefined) {
            newData.message = rawData.message
        }
        if (rawData.title !== undefined) {
            newData.title = rawData.title
        }
        if (rawData.roles !== undefined) {
            newData.roles = rawData.roles
        }
    }
    return newData
}

function ReadConfigMain(rawData) {
    let newData = {
        twitchClientID: ""
    }

    if (rawData !== undefined) {
        if (rawData.twitchClientID !== undefined) {
            newData.twitchClientID = rawData.twitchClientID
        }
    }
    return newData
}

function Init() {
    Commands.RegisterModuleName(__filename, "Go Live")
    Commands.AddCommand(__filename, "setgoliverole", "Sets the go live role.", "Either @ the role or use the display name.\nNote that you can add multiple roles sperated with a comma and `+`, `-`, or `=` for roles higher, lower, or equel(default) when compared to the given role.", "(.+)", "@Streamer", true, false, commandSetGoLiveRole)
    Commands.AddCommand(__filename, "setgolivechannel", "Sets the go live channel.", "Use in the channel or use # to mention the channel", "(.*)", "#Announcements", true, false, commandSetGoLiveChannel)
    let longDescription = "You can use the following and they will be replaced with their data: \n`{{game}}`\n`{{url}}`\n`{{username}}`\n`{{title}}`"
    Commands.AddCommand(__filename, "setgolivedescription", "Sets the go live message description.", longDescription, "(.+)", "{{url}}", true, false, commandSetGoLiveDescription)
    Commands.AddCommand(__filename, "setgolivemessage", "Sets the go live message.", longDescription, "(.+)", "{{url}}", true, false, commandSetGoLiveMessage)
    Commands.AddCommand(__filename, "setgolivetitle", "Sets the go live message title.", longDescription, "(.+)", "{{url}}", true, false, commandSetGoLiveTitle)

    ConfigManager.AddModuleConfig(__filename, ReadConfig)
    ConfigManager.AddModuleConfigMain(__filename, ReadConfigMain)

    EventListener.addListener("presenceUpdate", onPresenceUpdate)
}

module.exports = {
    Init
}