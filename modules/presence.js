const ConfigManager = require("../config_manager")
const EventListener = require("../event_listener")

function onReady(client) {
    let configMain = ConfigManager.GetModuleConfigMain(__filename)
    let presenceData = {
        game: {
            type: "PLAYING",
            name: ""
        }
    }
    let update = false

    if (configMain.status != "") {
        presenceData.status = configMain.status
        update = true
    }

    if (configMain.type != "") {
        presenceData.game.type = configMain.type
        presenceData.game.name = configMain.message
        update = true
    }

    if (update) {
        client.user.setPresence(presenceData).catch(console.error)
    }
}

function ReadConfigMain(rawData) {
    let newData = {
        status: "online",
        status_options: ["online", "idle", "offline", "dnd"],
        type: "PLAYING",
        type_options: ["PLAYING", "STREAMING", "LISTENING", "WATCHING"],
        message: ""
    }
    if (rawData !== undefined) {
        if (rawData.status !== undefined) {
            newData.status = rawData.status
        }
        if (rawData.type !== undefined) {
            newData.type = rawData.type
        }
        if (rawData.message !== undefined) {
            newData.message = rawData.message
        }
    }
    return newData
}

function Init() {
    ConfigManager.AddModuleConfigMain(__filename, ReadConfigMain)

    EventListener.addListener("ready", onReady)
}

module.exports = {
    Init
}