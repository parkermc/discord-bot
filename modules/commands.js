// TODO better perm system
const Discord = require("discord.js")
const path = require("path")

const ConfigManager = require("../config_manager")
const EventListener = require("../event_listener")
const Tools = require("../tools")

var commands = {}
var moduleNameMap = {}

function onMessage(client, msg) {
    if (!msg.author.bot) {
        let config = ConfigManager.GetModuleConfig(msg.guild.id, __filename)

        let reg = msg.content.match(new RegExp(`^${Tools.Escape(config.commandPrefix)}(.+?)\\b`))
        if (reg !== null && reg[1].toLowerCase() in commands && config.enabledCommands[reg[1].toLowerCase()]) {
            let command = commands[reg[1].toLowerCase()]
            let isAdmin = userIsAdmin(msg.member)
            if (!command.admin || isAdmin) {
                let regArgs = reg.input.substr(reg[0].length + 1, reg.input.length - reg[0].length).match(new RegExp("^" + command.regex_args + "$"))
                if (regArgs !== null) {
                    let args = []
                    for (let i = 1; i < regArgs.length; i++) {
                        args.push(regArgs[i])
                    }
                    command.func(client, msg, args, isAdmin)
                } else {
                    msg.channel.send("", new Discord.RichEmbed() // Send parameters error message
                        .setColor("#ff0000")
                        .setTitle("Error")
                        .setDescription(`There is an error with the parameters, use \`${config.commandPrefix}help ${reg[1]}\` for more info.`)
                    )
                }
            } else {
                msg.channel.send("", new Discord.RichEmbed() // Send perm error message
                    .setColor("#ff0000")
                    .setTitle("Error")
                    .setDescription("You must be an admin to use this command.")
                )
            }
        }
    }
}

function UserInRoles(member, rolesNeeded) {
    let roleManager = member.guild.roles

    for (let roleNeeded of rolesNeeded) {
        let rolePos = member.guild.roles.get(roleNeeded.id).position
        for (let role of member.roles) {
            if ((roleNeeded.mode == "=" && role[1].position == rolePos) ||
                (roleNeeded.mode == "+" && role[1].position >= rolePos) ||
                (roleNeeded.mode == "-" && role[1].position <= rolePos && (role[1].position != 0 || rolePos == 0))) {
                return true
            }
        }
    }

    return false
}

function ArgChannel(arg, guild) {
    let channelId = arg
    let pingreg = arg.match(/^<#(\d+)>$/)
    if (pingreg !== null) {
        channelId = pingreg[1]
    }
    for (let channel of guild.channels) {
        if (channelId == channel[1].id) {
            return channel[1]
        }
    }

    return null
}

function ArgRoles(arg, guild) {
    let foundRoles = []
    for (let argPart of arg.split(",")) {
        let found = false
        let roleInput = argPart.toLowerCase()
        let mode = "="
        let lastChar = roleInput.substr(roleInput.length - 1)
        if (lastChar == "+" || lastChar == "-" || lastChar == "=") {
            mode = lastChar
            roleInput = roleInput.substr(0, roleInput.length - 1)
        }
        let pingreg = roleInput.match(/^<@&(\d+)>$/)
        if (pingreg !== null) {
            roleInput = pingreg[1]
        }
        for (let role of guild.roles) {
            if (roleInput == role[1].id || role[1].name.toLowerCase() == roleInput) {
                foundRoles.push({
                    role: role[1],
                    mode: mode
                })
                found = true
                break
            }
        }

        if (!found) {
            return undefined
        }

    }
    if (foundRoles.length > 0) {
        return foundRoles
    }
    return undefined
}

function AddCommand(module_filename, name, description_short, description_long, regex_args, example_args, admin, autoEnabled, func) {
    let basename = path.basename(module_filename)
    let module_name = basename.substr(0, basename.length - 3)
    commands[name.toLowerCase()] = {
        admin,
        description_long,
        description_short,
        example_args,
        func,
        module: module_name,
        name: name.toLowerCase(),
        autoEnabled,
        regex_args
    }
}

function commandDisable(client, msg, args, admin) {
    let config = ConfigManager.GetModuleConfig(msg.guild.id, __filename)
    let command = args[0]
    if (!(command in commands) && command.substr(0, config.commandPrefix.length) == config.commandPrefix) {
        command = command.substr(config.commandPrefix.length, command.length - config.commandPrefix.length)

    }
    if (command != "enable" && command != "listdisabled") {
        if (command in commands) {
            ConfigManager.GetModuleConfig(msg.guild.id, __filename).enabledCommands[command] = false
            ConfigManager.SaveConfig(msg.guild.id)
            msg.channel.send("", new Discord.RichEmbed() // Send output message
                .setColor("#00ff00")
                .setDescription(`Command \`${config.commandPrefix+command}\` disabled.`)
            )
        } else {
            msg.channel.send("", new Discord.RichEmbed() // Send no command error message
                .setColor("#ff0000")
                .setTitle("Error")
                .setDescription(`Command \`${config.commandPrefix+command}\` does not exist.`)
            )
        }
    } else {
        msg.channel.send("", new Discord.RichEmbed() // Send can"t disable error message
            .setColor("#ff0000")
            .setTitle("Error")
            .setDescription(`Command \`${config.commandPrefix+command}\` cannot be disabled.`)
        )
    }
}

function commandEnable(client, msg, args, admin) {
    let config = ConfigManager.GetModuleConfig(msg.guild.id, __filename)
    let command = args[0]
    if (!(command in commands) && command.substr(0, config.commandPrefix.length) == config.commandPrefix) {
        command = command.substr(config.commandPrefix.length, command.length - config.commandPrefix.length)

    }
    if (command in commands) {
        ConfigManager.GetModuleConfig(msg.guild.id, __filename).enabledCommands[command] = true
        ConfigManager.SaveConfig(msg.guild.id)
        msg.channel.send("", new Discord.RichEmbed() // Send output message
            .setColor("#00ff00")
            .setDescription(`Command \`${config.commandPrefix+command}\` enabled.`)
        )
    } else {
        msg.channel.send("", new Discord.RichEmbed() // Send no command error message
            .setColor("#ff0000")
            .setTitle("Error")
            .setDescription(`Command \`${config.commandPrefix+command}\` does not exist.`)
        )
    }
}

function commandHelp(client, msg, args, admin) {
    let config = ConfigManager.GetModuleConfig(msg.guild.id, __filename)
    let isAdmin = userIsAdmin(msg.member)

    let commandsByModule = {}
    for (let command in config.enabledCommands) {
        if (config.enabledCommands[command] && (!commands[command].admin || isAdmin)) {
            let moduleName = commands[command].module
            if (!(moduleName in commandsByModule)) {
                commandsByModule[moduleName] = []
            }
            commandsByModule[moduleName].push(command)
        }
    }

    let all = false
    let catagory = null
    let command = null
    if (args[0] == "") {
        all = true
    } else {
        let lowerName = args[0]
        if (!(lowerName in commands) && lowerName.substr(0, config.commandPrefix.length) == config.commandPrefix) {
            lowerName = lowerName.substr(config.commandPrefix.length, lowerName.length - config.commandPrefix.length)

        }
        lowerName = lowerName.toLowerCase()
        for (let m in moduleNameMap) {
            if ((m.toLowerCase() == lowerName || moduleNameMap[m].toLowerCase() == lowerName) && commandsByModule[m] !== undefined) {
                catagory = m
                break
            }
        }
        for (let m in commandsByModule) {
            for (let commandM of commandsByModule[m]) {
                if (lowerName == commandM.toLowerCase()) {
                    command = commandM
                }
            }
        }
    }

    if (all || catagory != null) {
        for (let m of Object.keys(commandsByModule).sort()) {
            if (catagory == null || catagory == m) {
                let outMsg = new Discord.RichEmbed()
                    .setColor("#00CCFF")
                    .setTitle(`Help: ${getModuleName(m)}`)
                    .setDescription(`Use \`${config.commandPrefix}help <command name>\` for more infomation.`)
                for (let command of commandsByModule[m].sort()) {
                    outMsg.addField(command, commands[command].description_short, true)
                }
                msg.channel.send("", outMsg)
            }
        }
    } else if (command !== null) {
        let outMsg = new Discord.RichEmbed()
            .setColor("#00CCFF")
            .setTitle(`Help: ${command}`)
            .setDescription(`Use \`${config.commandPrefix}help\` for help on all commands.`)
            .addField("Example", `${config.commandPrefix+command} ${commands[command].example_args}`, true)
            .addField("Category", getModuleName(commands[command].module), true)
        if (commands[command].description_long == "") {
            outMsg.addField("Description", `${commands[command].description_short}`)
        } else {
            outMsg.addField("Description", `${commands[command].description_short}\n\n${commands[command].description_long}`)
        }
        msg.channel.send("", outMsg)
    } else {
        msg.channel.send("", new Discord.RichEmbed() // Send not found error message
            .setColor("#FF0000")
            .setTitle("Error")
            .setDescription(`Could not find any infomation on \`${args[0]}\`.`)
        )
    }
}

function commandListDisabled(client, msg, args, admin) {
    let config = ConfigManager.GetModuleConfig(msg.guild.id, __filename)

    let disabledByModule = {}
    for (let command in config.enabledCommands) {
        if (!config.enabledCommands[command]) {
            let moduleName = commands[command].module
            if (!(moduleName in disabledByModule)) {
                disabledByModule[moduleName] = []
            }
            disabledByModule[moduleName].push(command)
        }
    }

    for (let m of Object.keys(disabledByModule).sort()) {
        let outMsg = new Discord.RichEmbed()
            .setColor("#00CCFF")
            .setTitle(`Disabled Commands: ${getModuleName(m)}`)
            .setDescription(`The following commands are disabled.`)
        for (let command of disabledByModule[m].sort()) {
            outMsg.addField(command, commands[command].description_short, true)
        }
        msg.channel.send("", outMsg)
    }

    if (Object.keys(disabledByModule).length == 0) {
        msg.channel.send("", new Discord.RichEmbed()
            .setColor("#00CCFF")
            .setTitle("Disabled Commands")
            .setDescription(`No commands are disabled.`)
        )
    }
}

function commandSetAdmin(client, msg, args, admin) {
    let adminRoles = ArgRoles(args[0], msg.guild)
    if (adminRoles !== undefined) {
        if (adminRoles.length == 1) {
            let adminRole = adminRoles[0]
            ConfigManager.GetModuleConfig(msg.guild.id, __filename).adminRole = adminRole.role.id
            ConfigManager.SaveConfig(msg.guild.id)
            msg.channel.send("", new Discord.RichEmbed() // Send output message
                .setColor("#00ff00")
                .setDescription("Admin role set.")
                .addField("Role Name", adminRole.role.name, true)
                .addField("ID", adminRole.role.id, true)
                .addField("Mode", adminRole.mode, true)
            )
        } else {
            msg.channel.send("", new Discord.RichEmbed() // Send not found error message
                .setColor("#FF0000")
                .setTitle("Error")
                .setDescription(`Found more than one role matching \`${args[0]}\`.
            Try pinging the role.`)
            )
        }
    } else {
        msg.channel.send("", new Discord.RichEmbed() // Send not found error message
            .setColor("#FF0000")
            .setTitle("Error")
            .setDescription(`Error finding a role matching \`${args[0]}\`.`)
        )
    }
}

function commandSetPrefix(client, msg, args, admin) {
    ConfigManager.GetModuleConfig(msg.guild.id, __filename).commandPrefix = args[0]
    ConfigManager.SaveConfig(msg.guild.id)
    msg.channel.send("", new Discord.RichEmbed() // Send output message
        .setColor("#00ff00")
        .setDescription(`Command prefix set to \`${args[0]}\`.`)
    )
}

function getModuleName(moduleName) {
    if (moduleName in moduleNameMap) {
        return moduleNameMap[moduleName]
    }
    return module_name
}

function userIsAdmin(member) {
    let configMain = ConfigManager.GetModuleConfigMain(__filename)
    let config = ConfigManager.GetModuleConfig(member.guild.id, __filename)

    isAdmin = false
    for (let id of configMain.globalAdminIds) {
        if (id == member.user.id) {
            isAdmin = true
            break
        }
    }
    if (!isAdmin) {
        for (let role of member.roles) {
            if (role[0] == config.adminRole) {
                isAdmin = true
                break
            }
        }
    }
    return isAdmin
}

function ReadConfig(rawData) {
    let newData = {
        adminRole: "",
        commandPrefix: "$",
        enabledCommands: {}
    }

    for (let command in commands) {
        newData.enabledCommands[command] = commands[command].autoEnabled
    }

    if (rawData !== undefined) {
        if (rawData.adminRole !== undefined) {
            newData.adminRole = rawData.adminRole
        }
        if (rawData.commandPrefix !== undefined) {
            newData.commandPrefix = rawData.commandPrefix
        }
        if (rawData.enabledCommands !== undefined) {
            for (let command in newData.enabledCommands) {
                if (command in rawData.enabledCommands) {
                    newData.enabledCommands[command] = rawData.enabledCommands[command]
                }
            }
        }
    }
    return newData
}

function ReadConfigMain(rawData) {
    let newData = {
        globalAdminIds: []
    }
    if (rawData !== undefined) {
        if (rawData.globalAdminIds !== undefined) {
            newData.globalAdminIds = rawData.globalAdminIds
        }
    }
    return newData
}

function RegisterModuleName(module_filename, moduleName) {
    let basename = path.basename(module_filename)
    moduleNameMap[basename.substr(0, basename.length - 3)] = moduleName
}

function Init() {
    RegisterModuleName(__filename, "Commands")
    AddCommand(__filename, "disable", "Disables a command.", "Commands can be enabled with the `enable` command.", "([^\\s]+)", "setprefix", true, true, commandDisable)
    AddCommand(__filename, "enable", "Enables a command.", "Commands can be disabled with the `disable` command.", "([^\\s]+)", "setprefix", true, true, commandEnable)
    AddCommand(__filename, "listdisabled", "Lists the disabled commands.", "To enable and disable commands use `enable` and `disable` commands.", "", "", true, true, commandListDisabled)
    AddCommand(__filename, "setadmin", "Sets the admin role.", "Either @ the role or use the display name", "(.+)", "@Admin", true, true, commandSetAdmin)
    AddCommand(__filename, "setprefix", "Sets the command prefix for the bot to use.", "", "([^\\s]+)", "$", true, true, commandSetPrefix)

    AddCommand(__filename, "help", "Show help for the commands.", "Can be used to show help on all commands, one catagory, or one command.", "([^\\s]*)", "help", false, false, commandHelp)

    ConfigManager.AddModuleConfig(__filename, ReadConfig)
    ConfigManager.AddModuleConfigMain(__filename, ReadConfigMain)

    EventListener.addListener("message", onMessage)
}

module.exports = {
    AddCommand,
    ArgChannel,
    ArgRoles,
    Init,
    RegisterModuleName,
    UserInRoles
}