/**
 * The starting file for the bot
 * 
 * Starts and sets up the bot forwards the events with more argument
 */
const Discord = require("discord.js")
const fs = require("fs")
const path = require("path")

// Load any "core" modules
const ConfigManager = require("./config_manager")
const EventListener = require("./event_listener")

const client = new Discord.Client()
const modulesFolder = path.join(__dirname, "modules")

let modules = {} // Store all of the modules

// Load all of the modules
let files = fs.readdirSync(modulesFolder)
for (let file of files) {
    if (file.endsWith(".js")) {
        modules[file.substr(0, file.length - 3)] = require(path.join(modulesFolder, file))
    }
}

// Init all the modules
for (let m of Object.keys(modules).sort()) {
    if (modules[m].Init !== undefined) {
        modules[m].Init()
    }
}

// Create all the listener forwarders
client.on("message", (msg) => {
    EventListener.emit("message", client, msg)
})

client.on("presenceUpdate", (oldMember, newMember) => {
    EventListener.emit("presenceUpdate", client, oldMember, newMember)
})

client.on("ready", () => {
    console.log(`Logged in as ${client.user.tag}!`)
    EventListener.emit("ready", client)
})

// Get the main config and start the bot
var configMain = ConfigManager.GetConfigMain()
client.login(configMain.token)