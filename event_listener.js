/**
 * Creates a new event emmiter to deliver more args with each event
 * 
 * Exports: The event emitter
 */
const events = require('events')

module.exports = new events.EventEmitter()